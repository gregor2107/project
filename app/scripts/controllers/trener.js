(function () {
	'use strict';

	angular.module('module')
		.controller('TrenerCtrl', [ '$sessionStorage', '$rootScope', 'dataService', '$scope', function ($sessionStorage, $rootScope, dataService, $scope) {
			
			$rootScope.logged = $sessionStorage.logged;
			this.isLogged = $sessionStorage.logged;

			$scope.rating = 4;
		
		    $scope.rateFunction = function(rating) {
		      console.log('Rating selected: ' + rating);
		    };

			dataService.getUsers().then(function(res){
				console.log(res);
				$scope.klient = res;

				for (var i in res){
					if(!res[i].avatar){
						$scope.klient[i].avatar = 'emptyAvatar.jpg';
					}
				}
			})

			dataService.getCoaches().then(function(res){
				console.log(res);
				$scope.trenerzy = res;
				var tmp = [];
				for (var i in res){
					if(!res[i].avatar){
						$scope.trenerzy[i].avatar = 'emptyAvatar.jpg';
					}
					$scope.trenerzy[i].club_id = String(res[i].club_id);
					if($scope.trenerzy[i].club_id == $sessionStorage.club_id){
						tmp.push($scope.trenerzy[i]);
					}
					else if($sessionStorage.club_id == null){
						tmp.push($scope.trenerzy[i]);

					}
				}
				$scope.trenerzy = tmp;
			})

			$scope.getCoaches = function(){
				dataService.getCoaches().then(function(res){
					console.log(res);
					$scope.trenerzy = res;
					var tmp = [];
					for (var i in res){
						if(!res[i].avatar){
							$scope.trenerzy[i].avatar = 'emptyAvatar.jpg';
						}
						$scope.trenerzy[i].club_id = String(res[i].club_id);
						if($scope.trenerzy[i].club_id == $sessionStorage.club_id){
							tmp.push($scope.trenerzy[i]);
						}
					}
					$scope.trenerzy = tmp;
				})
			}

			dataService.getClubs().then(function(res){
				$scope.clubs = res;
			})

			dataService.getDictionaries().then(function(res){
				console.log(res.user.levels);
				$scope.poziomy = res.user.levels;
			})

			$scope.saveUser = function(updateKlient){

				console.log(updateKlient);
		
        		// if($scope.form.$valid){

					var params = {
						first_name: updateKlient.first_name,
						last_name: updateKlient.last_name,
						birthdate: String(updateKlient.birthdate),
						phone_number: String(updateKlient.phone_number),
						email: updateKlient.email,
						level: updateKlient.level,
						seniority_level: updateKlient.seniority_level,
						description: updateKlient.description
					};

					dataService.saveUsers(params,updateKlient.id);
					
			    	//$scope.close();
		    	// }
		    }

			$scope.ageCounter = function(){
		    	
		    	var a = moment();
		    	var b = $scope.updateKlient.birthdate;
				var age = a.diff(b, 'years');
				console.log(age);
	  		
				if(age<18){
					console.log("Junior");
					$scope.updateKlient.seniority_level = 'Junior';
				}else if(age>=18 && age<=34){
					console.log("Adult");
					$scope.updateKlient.seniority_level = 'Adult';
				}else if(age>=35 && age<=45){
					console.log("Young Senior");
					$scope.updateKlient.seniority_level = 'Young Senior';
				}else if(age>=45 && age<=60){
				    console.log("Senior");
					$scope.updateKlient.seniority_level = 'Senior';
				}else if(age>=61){
				 	console.log("Super Senior");
				 	$scope.updateKlient.seniority_level = 'Super Senior';
				}

		    }

		    $scope.trenerSelect = function(select){
		    	$scope.selectTrener = {};
		    	$scope.selectTrener = select;
		    }

		}]);
})();